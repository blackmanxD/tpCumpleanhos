#include <stdio.h>
#include "sha1.c"
#include <stdlib.h>
#include <string.h>
#include "frases.c"

#define CHAR_BITS 8
#define TAM 256

void print_binint(uint8_t num);
void imprimir_resultados(uint8_t vector[], FILE* nombre_archivo_salida);
void hashs(char*nombre_archivo_entrada, char*nombre_archivo_salida);
void comparar_hashs(char * nombre_archivo1, char* nombre_archivo2);

int main(){
  system("clear");
  printf("\e[36m\e[1mpaso 1 - Generación de los mensajes\e[0m\n");

  char *variable [2][9];

  importar_variables(variable,"./archivos/frases_positivas");
  crear_cadenas(variable,"./archivos/oraciones_positivas");
  importar_variables(variable,"./archivos/frases_negativas");
  crear_cadenas(variable,"./archivos/oraciones_negativas");

  SHA1Context dato;
  system("rm ./archivos/hashs_positivos");
  system("rm ./archivos/hashs_negativos");

  printf("\E[36m\e[1m\nPaso 2 - Cálculo de las funciones hash\E[m\n" );

  hashs("archivos/oraciones_positivas","archivos/hashs_positivos");
  hashs("archivos/oraciones_negativas","archivos/hashs_negativos");

  printf("\E[36m\e[1m\nPaso 3 - Comparación de los valores de la función hash.. \e[m\n\n" );

  comparar_hashs("./archivos/hashs_positivos","./archivos/hashs_negativos");
  return 0;
}

void comparar_hashs(char * nombre_archivo1, char* nombre_archivo2)
{
        char hash1[257][40],hash2[257][40];

        FILE *archivo1, *archivo2,*archivo_final;
        if( (archivo1 = fopen(nombre_archivo1,"r") ) == NULL )
            printf("no se pudo abrir el archivo, probablemente esta siendo utilizado por otro programa o no existe\n" );
        if( (archivo2 = fopen(nombre_archivo2,"r") ) == NULL )
            printf("no se pudo abrir el archivo, probablemente esta siendo utilizado por otro programa o no existe\n" );

        int i,j,k,c;
        i=0;
        uint8_t datos;
        for(i=0; i < TAM ; i++)
        {

            fscanf(archivo1,"%[^\n]\n", hash1[i]);
            fscanf(archivo2,"%[^\n]\n", hash2[i]);
        }
        int a,b;
        if( (archivo_final = fopen("./archivos/cartas_intercambiables","w") ) == NULL )
            printf("no se pudo abrir el archivo, probablemente esta siendo utilizado por otro programa o no existe\n" );

        for(i = 0 ; i < TAM ; i++)
        {
            for(j = 0 ; j < TAM ; j++)
            {
                c=0;
                for(k = 36 ;k < 40 ; k++)
                {
                    if( hash1[i][k] == hash2[j][k] )
                    {
                        c++;
                    }
                    else
                        continue;
                }
                if (c == 4 )
                {
                    // printf("El hash de la oracion positiva \e[1m\e[92m%d\e[m y de la oracion negativa \e[1m\e[92m%d\e[m coinciden en sus ultimos 16 bits\n\n",i+1,j+1 );
                    fprintf(archivo_final,"El hash de la oracion positiva %d y de la oracion negativa %d coinciden en sus ultimos 16 bits\n\n",i+1,j+1 );
                    for(k = 0 ; k < 40 ; k++)
                        fprintf(archivo_final,"%c", hash1[i][k]);
                    fprintf(archivo_final,"\n" );
                    for(k = 0 ; k < 40 ; k++)
                        fprintf(archivo_final,"%c", hash2[j][k]);
                    fprintf(archivo_final,"\n\n" );

                }

            }
        }
        fclose(archivo1);
        fclose(archivo2);
}

void hashs(char *nombre_archivo_entrada, char* nombre_archivo_salida)
{
    int contador = 0;
    char mensaje[500];
    SHA1Context dato;
    uint8_t resultado[SHA1HashSize];
    FILE *archivo_entrada,*archivo_salida;
    if( (archivo_entrada = fopen(nombre_archivo_entrada,"r") ) == NULL )
    {
        printf("no se pudo abrir el archivo, probablemente esta siendo utilizado por otro programa o no existe\n" );
    }

    int tamanho_mensaje;
    if( (archivo_salida = fopen(nombre_archivo_salida,"a") ) == NULL )
    {
        printf("no se pudo abrir el archivo, probablemente esta siendo utilizado por otro programa o no existe\n" );
    }

    while(feof(archivo_entrada) == 0)
    {
        fscanf(archivo_entrada,"%[^\n]\n",mensaje);
        tamanho_mensaje = strlen(mensaje);
        // printf("el tamanho del mensaje es:  %d\n", tamanho_mensaje);
        SHA1Reset(&dato);
        SHA1Input(&dato,mensaje,tamanho_mensaje);
        SHA1Result(&dato,resultado);
        // printf("el contador va en :  %d\n", contador);
        imprimir_resultados(resultado,archivo_salida);
        contador++;
    }
    fclose(archivo_entrada);
    fclose(archivo_salida);
}

void imprimir_resultados(uint8_t vector[], FILE* archivo)
{
    int i;
    uint8_t datos;
    for( i = 0 ; i < 20 ; i++)
    {
        datos=vector[i];
        if(datos<16)
        {
            fprintf(archivo, "0%x", datos);
            // printf("0%x", datos);
        }
        else
        {
            fprintf(archivo, "%x", datos);
            // printf("%x", datos);
        }
    }
    fprintf(archivo, "\n");
    // printf("\n");
}
