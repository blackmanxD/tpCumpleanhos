
void crear_cadenas(char *variable[2][9], char nombre_archivo[]);
void importar_variables(char *variable[2][9], char nombre_archivo[]);


void importar_variables(char *variable[2][9], char nombre_archivo[])
{
    FILE *archivo_entrada;
    if (NULL == ( archivo_entrada = fopen(nombre_archivo,"r")) )
        printf("no se pudo abrir el archivo %s\n",nombre_archivo);

    int i=0;
    printf("\nImportando variables del archivo \e[1m\%s\e[m\n", nombre_archivo);
    while (feof(archivo_entrada) == 0) {
        char aux[30],aux2[30];
        fscanf(archivo_entrada , "%[^,], %[^\n]\n" , aux , aux2);
        variable[0][i] = (char*)malloc(strlen(aux)*sizeof(char));
        strcpy(variable[0][i],aux);

        variable[1][i] =  (char*)malloc(strlen(aux2)*sizeof(char));
        strcpy(variable[1][i],aux2);
        printf("%-20s\t\t%s\n",variable[0][i],variable[1][i] );
        i++;
    }
    fclose(archivo_entrada);
}


void crear_cadenas(char *variable[2][9], char nombre_archivo[])
{
    int a,b,i,j,k,l,m,n,o,p,q,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;
    char cadena[600];
    FILE *archivo_salida;
    if (NULL == ( archivo_salida = fopen(nombre_archivo,"w")) )
        printf("no se pudo abrir el archivo %s\n",nombre_archivo);

    for(j=0 ; j<2 ; j++)
    {
        if(j==1)
            cadena[r]='\0';
        r = strlen(cadena);
        cadena[0]='\0';
        strcat(cadena,variable[j][0]);
        for(k=0 ; k<2 ; k++)
        {
            if(k==1)
                cadena[s]='\0';
            s = strlen(cadena);
            strcat(cadena," profesor, me dirijo a ustedes con el ");
            strcat(cadena,variable[k][1]);
            for(l=0 ; l<2 ; l++)
            {
                if(l==1)
                    cadena[t]='\0';
                t = strlen(cadena);
                strcat(cadena," de ");
                strcat(cadena,variable[l][2]);
                for(m=0 ; m<2 ; m++)
                {
                    if(m==1)
                        cadena[u]='\0';
                    u = strlen(cadena);
                    strcat(cadena," de su ");
                    strcat(cadena,variable[m][3]);
                    for(n=0 ; n<2 ; n++)
                    {
                        if(n==1)
                            cadena[v]='\0';
                        v = strlen(cadena);
                        strcat(cadena," y aprovecho para ");
                        strcat(cadena,variable[n][4]);
                        for(o=0 ; o<2 ; o++)
                        {
                            if(o==1)
                                cadena[w]='\0';
                            w = strlen(cadena);
                            strcat(cadena,", dicho ");
                            strcat(cadena,variable[o][5]);
                            for(p=0 ; p<2 ; p++)
                            {
                                if(p==1)
                                    cadena[x]='\0';
                                x = strlen(cadena);
                                strcat(cadena," se hara ");
                                strcat(cadena,variable[p][6]);
                                for(q=0 ; q<2 ; q++)
                                {
                                    if(q==1)
                                        cadena[y]='\0';
                                    y = strlen(cadena);
                                    strcat(cadena,variable[q][7]);
                                    strcat(cadena," .");
                                    // printf("%s\n",cadena );
                                    fprintf(archivo_salida, "%s\n", cadena );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    fclose(archivo_salida);
}
